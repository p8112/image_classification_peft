from datasets import load_dataset, DatasetDict
import yaml
from typing import Dict, List
from transformers import (
    AutoImageProcessor
)
from transformers.modeling_outputs import (
    ImageClassifierOutput
)
from transformers import AutoModelForImageClassification
from peft import LoraConfig, get_peft_model, LoraModel
import torch
from torch import Tensor
from torchmetrics.classification import (
    MulticlassAccuracy, MulticlassF1Score
)
from torch.utils.data import DataLoader
from torch.optim import AdamW
from pathlib import Path
from tqdm import tqdm


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


Path(config['output_dir']).mkdir(parents=True, exist_ok=True)


dataset: DatasetDict = load_dataset(path=config["dataset_path"])
label_names: List[str] = dataset["train"].features["label"].names
label_to_id: Dict[str, int] = {
    label: idx for idx, label in enumerate(label_names)
}
id_to_label: Dict[int, str] = {
    idx: label for idx, label in enumerate(label_names)
}


image_processor = AutoImageProcessor.from_pretrained(
    pretrained_model_name_or_path=config["pretrained_model"]
)


def collate_fn(batch: List[Dict]) -> Dict[str, Tensor]:
    """
    Collate fn
    :return: dict contains
        - pixel_values
        - labels
    """
    pixel_values = image_processor(
        images=[
            instance["image"].convert("RGB") for instance in batch
        ],
        return_tensors="pt"
    )["pixel_values"]
    labels = torch.tensor(
        [instance["label"] for instance in batch],
        dtype=torch.long
    )
    return {
        "pixel_values": pixel_values,
        "labels": labels
    }


train_dataloader = DataLoader(
    dataset=dataset["train"], batch_size=config["batch_size"],
    num_workers=config["num_workers"], shuffle=True, collate_fn=collate_fn
)
validation_dataloader = DataLoader(
    dataset=dataset["validation"], batch_size=config["batch_size"],
    num_workers=config["num_workers"], shuffle=False, collate_fn=collate_fn
)
test_dataloader = DataLoader(
    dataset=dataset["test"], batch_size=config["batch_size"],
    num_workers=config["num_workers"], shuffle=False, collate_fn=collate_fn
)


device = torch.device(config["device"])
model = AutoModelForImageClassification.from_pretrained(
    pretrained_model_name_or_path=config["pretrained_model"],
    label2id=label_to_id, id2label=id_to_label
).to(device)
peft_config = LoraConfig(
    r=config["lora_r"], lora_alpha=config["lora_alpha"],
    lora_dropout=config["lora_dropout"], bias="none",
    target_modules=config["lora_target_modules"],
    modules_to_save=["classifier"],
)
model: LoraModel = get_peft_model(
    model=model, peft_config=peft_config
)
model.print_trainable_parameters()
optimizer = AdamW(
    params=model.parameters(), lr=config["learning_rate"]
)


def train_epoch(epoch: int) -> float:
    """
    Execute a training epoch
    :param epoch: epoch to train
    :return: loss
    """
    model.train()

    step_losses: List = []
    progress_bar = tqdm(
        iterable=enumerate(train_dataloader),
        desc=f"Training epoch {epoch} ..."
    )
    for step, batch in progress_bar:
        outputs: ImageClassifierOutput = model(
            pixel_values=batch["pixel_values"].to(device),
            labels=batch["labels"].to(device)
        )
        loss = outputs.loss

        loss.backward()
        if (step + 1) % config["accumulate_gradient_batches"] == 0:
            optimizer.step()
            optimizer.zero_grad()

        loss_value: float = loss.item()
        step_losses.append(loss_value)
        if (step + 1) % config["update_bar_interval"] == 0:
            progress_bar.set_description(
                f"Training epoch {epoch}: loss {loss_value:.6f} ..."
            )
            progress_bar.update()
    progress_bar.close()

    return sum(step_losses) / len(step_losses)


accuracy_fn = MulticlassAccuracy(num_classes=len(label_to_id))
f1_fn = MulticlassF1Score(num_classes=len(label_to_id))


@torch.no_grad()
def eval_epoch(dataloader: DataLoader, epoch: int):
    """
    Compute evaluate
    :param dataloader:
    :param epoch:
    :return:
    """
    model.eval()

    all_predicts: List[Tensor] = []
    all_labels: List[Tensor] = []

    progress_bar = tqdm(
        iterable=enumerate(dataloader),
        desc=f"Evaluate epoch {epoch} ..."
    )
    for step, batch in progress_bar:
        outputs: ImageClassifierOutput = model(
            pixel_values=batch["pixel_values"].to(device),
            labels=batch["labels"].to(device)
        )
        logits = outputs.logits
        predicts = torch.argmax(logits, dim=1)

        all_labels.append(batch["labels"])
        all_predicts.append(predicts.cpu().detach())
    progress_bar.close()

    all_predicts: Tensor = torch.cat(all_predicts, dim=0)
    all_labels: Tensor = torch.cat(all_labels, dim=0)

    return {
        "f1": f1_fn(all_predicts, all_labels).item(),
        "accuracy": accuracy_fn(all_predicts, all_labels).item()
    }


best_f1: float = 0.0
for i in range(config["num_epochs"]):
    train_loss: float = train_epoch(epoch=i)
    print(f"Finish training epoch {i}: loss {train_loss:.6f}")
    torch.save(
        model.state_dict(),
        f'{config["output_dir"]}/model.pt'
    )

    tmp: Dict[str, float] = eval_epoch(
        dataloader=validation_dataloader, epoch=i
    )
    print(f"Finish validation epoch {i}: f1 {tmp['f1']:.6f} accuracy {tmp['accuracy']:.6f}")
    if best_f1 < tmp["f1"]:
        print(f"F1 score improve from {best_f1:.6f} to {tmp['f1']:.6f}")
        best_f1 = tmp["f1"]
        torch.save(model.state_dict(), f"{config['output_dir']}/model.pt")


model.load_state_dict(
    torch.load(f"{config['output_dir']}/model.pt")
)
tmp: Dict[str, float] = eval_epoch(
    dataloader=test_dataloader, epoch=0
)
print(f"Finish test: f1 {tmp['f1']:.6f} accuracy {tmp['accuracy']:.6f}")
