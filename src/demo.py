import numpy as np
import streamlit as st
import yaml
import torch
from typing import Dict, List
import torch.nn as nn
from transformers import (
    AutoModelForImageClassification,
    AutoImageProcessor
)
from transformers.modeling_outputs import (
    ImageClassifierOutput
)
from peft import (
    LoraConfig, get_peft_model, LoraModel
)
from datasets import load_dataset, DatasetDict
from PIL import Image


st.set_page_config(
    page_title="Image classification demo",
    layout="centered"
)


st.title("Image classification fine-tuned with peft demo")
st.markdown("---")


@st.cache_resource
def load_config() -> Dict:
    """
    Load config
    :return:
    """
    with open("config.yaml", mode="r") as file_obj:
        result: Dict = yaml.safe_load(file_obj)
    return result


config: Dict = load_config()
device = torch.device(config["device"])


@st.cache_resource
def get_label_to_id() -> Dict[str, int]:
    """
    Get label to id
    :return:
    """
    dataset: DatasetDict = load_dataset(path=config["dataset_path"])
    label_names: List[str] = dataset["train"].features["label"].names
    return {
        label: idx for idx, label in enumerate(label_names)
    }


@st.cache_resource
def load_model() -> nn.Module:
    """
    Load model
    :return:
    """
    label_to_id: Dict[str, int] = get_label_to_id()
    id_to_label: Dict[int, str] = {
        idx: label for label, idx in label_to_id.items()
    }
    result = AutoModelForImageClassification.from_pretrained(
        pretrained_model_name_or_path=config["pretrained_model"],
        label2id=label_to_id, id2label=id_to_label
    ).to(device)
    peft_config = LoraConfig(
        r=config["lora_r"], lora_alpha=config["lora_alpha"],
        lora_dropout=config["lora_dropout"], bias="none",
        target_modules=config["lora_target_modules"],
        modules_to_save=["classifier"],
    )
    result: LoraModel = get_peft_model(
        model=result, peft_config=peft_config
    )
    result.load_state_dict(
        state_dict=torch.load(
            f"{config['output_dir']}/model.pt", map_location=device
        )
    )
    result.eval()
    return result


@st.cache_resource
def load_processor():
    """
    Load processor
    :return:
    """
    return AutoImageProcessor.from_pretrained(
        pretrained_model_name_or_path=config["pretrained_model"]
    )


uploaded_file = st.file_uploader("Choose an image")
st.markdown("---")


if uploaded_file is not None:
    processor = load_processor()
    model = load_model()

    pil_image = Image.open(uploaded_file).convert('RGB')
    tensor_image = processor(
        images=[pil_image], return_tensors="pt"
    )["pixel_values"]

    with torch.no_grad():
        outputs: ImageClassifierOutput = model(
            pixel_values=tensor_image.to(device)
        )
        logits = outputs.logits
        predict = torch.argmax(logits, dim=1).cpu().detach().tolist()[0]

    predict = model.config.id2label[predict]

    st.subheader("Uploaded image")
    with st.columns(spec=3)[1]:
        st.image(np.asarray(pil_image))

    st.subheader("Predict")
    st.info(predict)
